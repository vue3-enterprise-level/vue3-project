import { SlateDescendant, SlateElement, SlateText } from "@wangeditor/editor-for-vue";

declare module "@wangeditor/editor-for-vue" {
  // 扩展 Text
  interface SlateText {
    text: string;
  }
  interface SlateImageElement {
    src: string;
    alt: string;
    url: string;
    href: string;
  }

  interface SlateVideoElement {
    src: string;
    poster?: string;
  }

  type InsertFnType = (url: string, alt: string, href: string) => void;

  // 扩展 Element
  interface SlateElement {
    type: string;
    children: SlateDescendant[];
  }
}
