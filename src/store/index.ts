import { createPinia } from "pinia";
import type { App } from "vue";
import { useRoutingTagStore } from "./modules/routingTag";
import { useMenuStore } from "./modules/menu";
import { useAddRouteStore } from "./modules/addRoute";

const store = createPinia();

export function setupStore(app: App<Element>) {
  app.use(store);
  // 如果有缓存记录直接动态加载路由
  useAddRouteStore().setWhiteList();
  useAddRouteStore().recurseAddRoute(useAddRouteStore().whiteList);
}

export {
  store,
  useRoutingTagStore,
  useMenuStore,
  useAddRouteStore,
};
