// 关于路由tabs相关数据以及操作
import { defineStore } from "pinia";
import type { RouteLocationNormalized } from "vue-router";
import type { RouterList } from "./types";
export const useRoutingTagStore = defineStore({
  id: "routingTag", // id必填，且需要唯一
  state: (): RouterList => {
    return {
      routerList: [
        {
          name: "Dashboard",
          title: "首页",
          path: "/dashboard",
          active: true,
        },
      ],
      active: { name: "", path: "", title: "" },
      activeRouter: ["Dashboard"],
    };
  },
  actions: {
    // 根据路由路径移除路由标签对应的数据
    removeRouterPath(path: string) {
      this.routerList.forEach((item, index) => {
        if (item.path === path) {
          if (item.active) {
            this.routerList[index + 1]
              ? (this.routerList[index + 1].active = true)
              : (this.routerList[index - 1].active = true);
          }
          this.routerList.splice(index, 1);
          this.updatedKeepRouter();
        }
      });
    },
    // 根据路由名称移除路由标签对应的数据
    removeRouterName(name: string) {
      this.routerList.forEach((item, index) => {
        if (item.name === name) {
          if (item.active) {
            this.routerList[index + 1]
              ? (this.routerList[index + 1].active = true)
              : (this.routerList[index - 1].active = true);
          }
          this.routerList.splice(index, 1);
          this.updatedKeepRouter();
        }
      });
    },
    // 更新 当前路由标签页选中状态
    updatedRouter(path: string, name?: string) {
      this.setRouterStatusFalse();
      this.routerList.forEach((item, index) => {
        if (item.path === path) {
          this.routerList[index].active = true;
        }
        if (item.name === name && item.path != path) {
          this.routerList[index].active = true;
          this.routerList[index].path = path;
        }
        this.updatedKeepRouter();
      });
    },
    // 更新activeRouter数据  keepalive 缓存组件
    updatedKeepRouter() {
      this.activeRouter = this.routerList.map((item) => item.name?item.name:'');
    },
    // 取消缓存路由，以及删除路由标签页
    onDisablingCacheRouting(name: string) {
      this.routerList.splice(
        this.routerList.findIndex((item) => item.name === name),
        1
      );
      this.updatedKeepRouter();
    },
    // 默认素所有路由标签不选中
    setRouterStatusFalse() {
      this.routerList.forEach((item) => {
        item.active = false;
      });
      this.updatedKeepRouter();
    },
    // 新增路由标签数据
    addRouterData(name: string, title: string, path: string) {
      this.setRouterStatusFalse();
      if (!this.routerList.some((item) => item.name === name)) {
        this.routerList.push({
          name: name,
          title: title,
          path: path,
          active: true,
        });
        this.updatedKeepRouter();
      } else {
        this.updatedRouter(path, name);
      }
    },
    // 递归路由
    routerRecursive(arr: any, num: number, paths: any, path: string) {
      return arr.filter((item: any) => {
        if (
          item.path.indexOf(paths[paths.length - num]) != -1 &&
          item.path.length - paths[paths.length - num].length <= 1
        ) {
          if (num && item.children && (item.children.length as number)) {
            num--;
            return this.routerRecursive(item.children, num, paths, path);
          } else {
            this.addRouterData(item.name, item.meta.title, path);
            return true;
          }
        }
      });
    },
    // 设置选中标签
    setActive(to: RouteLocationNormalized) {
      if (to.path != "/login") {
        this.active.name = to.name as string;
        this.active.path = to.path;
        this.active.title = to.meta.title as string;
        this.addRouterData(
          this.active.name,
          this.active.title,
          this.active.path
        );
      }
    },
  },
});
