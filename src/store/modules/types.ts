import { RouteRecordRaw } from 'vue-router';
export interface CountInterface {
  count: number
}
interface RouterType {
  name: string;
  title: string;
  active: true | false;
  path: string;
}
export interface RouterList {
  routerList: RouterType[];
  activeRouter:string[];
  active:activeTabs
}
export interface menu {
  name: string;
  path: string;
  title: string;
  icon?:string;
  children?: menu[];
}
export interface activeTabs {
  name:string;
  path:string;
  title:string;
}
export interface menuList {
  menuList: menu[];
  baseRouter?: RouteRecordRaw[];
}
export interface AddRouteType {
  whiteList:RouteRecordRaw[]
} 
