//post类型接口数据导出
// @params  res 响应数据   name 导出文件名
export default (res: any, name: string) => {
  // Blob 对象表示一个不可变、原始数据的类文件对象
  const blob = new Blob([res], { type: "application/force-download" });
  // FileReader 对象允许Web应用程序异步读取存储在用户计算机上的文件的内容
  const fileReader = new FileReader();
  fileReader.readAsDataURL(blob);
  //开始读取指定的Blob中的内容。一旦完成，result属性中将包含一个data: URL格式的Base64字符串以表示所读取文件的内容
  fileReader.onload = (e) => {
    let a = document.createElement("a");
    a.download = `${name}.xlsx`;
    let event = e.target as any;
    a.href = event.result as string;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  };
};
