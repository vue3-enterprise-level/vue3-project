import type { App } from "vue";
// 引入elemnt plus样式
import "element-plus/dist/index.css";
import ElementPlus from "element-plus";
import zhCn from "element-plus/es/locale/lang/zh-cn";
// 从 @element-plus/icons-vue 中导入所有图标并进行全局注册。
import * as ElementPlusIconsVue from "@element-plus/icons-vue";
export function setupElement(app: App) {
  // 设置element plus 中文
  app.use(ElementPlus, {
    locale: zhCn,
  });
  // 注册所有图标
  for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component);
  }
}
