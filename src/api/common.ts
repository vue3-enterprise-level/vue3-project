import request from '/@/service'

export interface CommonRequestType {
  url: string;
}
// 通用url请求
export const commonRequest = (params: CommonRequestType) => {
  return request({
    url: params.url,
    method: "get",
  });
};