import { createApp } from "vue";
import { setupRouter } from "./router";
import App from "./App.vue";
import { setupStore } from "./store";
import {setupElement} from "/@/settings/elementSettings";
// 引入animate.css
import 'animate.css';
// 引入全局样式
import "./assets/css/index.css";
// 创建vue实例
const app = createApp(App);
// 配置pinia
setupStore(app);
// 配置路由
setupRouter(app);
// 配置element plus
setupElement(app);

// 挂载实例
app.mount("#app");
