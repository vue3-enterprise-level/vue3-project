import { RouteRecordRaw } from "vue-router";

// 权限路由
export const permissionsRoutes: Array<RouteRecordRaw> = [
  {
    path: "/dashboard",
    name: "Dashboard",
    component: () => import("/@/views/dashboard/index.vue"),
    children: [],
    meta: {
      title: "首页",
      icon:"HomeFilled",
      isMenu: true,
      component: "/views/dashboard/index.vue",
    },
  },
  {
    path: "/richTextPage",
    name: "RichTextPage",
    component: () => import("/@/views/richTextPage/index.vue"),
    children: [],
    meta: {
      title: "富文本组件",
      icon:"HomeFilled",
      isMenu: true,
      component: "/views/richTextPage/index.vue",
    },
  },
  {
    path: "/demo",
    name: "Demo",
    component: () => import("/@/components/page/PageView.vue"),
    redirect: "/demo/test",
    children: [
      {
        path: "test",
        name: "Test",
        meta: {
          title: "test",
          icon:"",
          isMenu: true,
          component: "/views/demo/test/index.vue",
        },
        component: () =>
          import("/@/views/demo/test/index.vue"),
      },
    ],
    meta: {
      title: "demo",
      icon:"HomeFilled",
      isMenu: true,
      component: "/components/page/PageView.vue",
    },
  },
];

// 基础路由
export const baseRoutes: Array<RouteRecordRaw> = [
  {
    path: "/:catchAll(.*)",
    redirect: "/login",
    meta: {
      title: "重定向",
    },
  },
  {
    name: "Login",
    path: "/login",
    component: () => import("/@/views/sys/login/index.vue"),
    meta: {
      title: "登陆",
    },
  },
  {
    path: "/",
    redirect: "/dashboard",
    name: "Root",
    component: () => import("/@/layouts/default/index.vue"),
    children: [],
  },
];
