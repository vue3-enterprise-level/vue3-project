import {
  createRouter,
  createWebHistory,
  createWebHashHistory,
} from "vue-router";
import type { App } from "vue";
import { baseRoutes } from "./modules/routes";
import { useAddRouteStore, useRoutingTagStore } from "/@/store";

import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

// 配置路由
export const router = createRouter({
  history: createWebHashHistory(),
  routes: [...baseRoutes],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { top: 0 };
    }
  },
});

// 路由守卫
router.beforeEach((to, from) => {
  NProgress.start() // 进度条开始
  useRoutingTagStore().setActive(to);
  if (
    !useAddRouteStore().whiteList.length &&
    to.path != "/login"
  ) {
    return "/login";
  } else {
    return true;
  }
});

router.afterEach(() => {
  NProgress.done() // 进度条结束
})


export function setupRouter(app: App<Element>) {
  app.use(router);
}
