# vue3 template

一个 Vue3 + TS + Vite + Element-Plus 的企业级开发模板。


## 特性

1. 这个是 **Vue3.2**的模板
2. 支持 **Vite**
3. UI框架是 **Element-Plus** 
4. CSS框架是 **TailwindCSS**
5. 状态管理是 **Pinia**
6. **ESlint+Prettier** 已经配置完成
7. Git **提交前验证**以及**提交规范**已做好


## clone

这个是创建项目的第一步
使用 `git clone` 命名，命令如下：
```bash
git clone https://github.com/ywanzhou/vue3-template.git
```

方式二：自行发挥~。

## 启动项目

这里我使用的包管理工具时npm，node版本大于或等于 `v16.15.0`。

现在开始正式启动这个项目

1. 安装依赖
```bash
npm install # 或者 npm i 或者 yarn
```
2. 开发过程
```bash
npm run dev # 运行

npm run dev:open # 自动打开浏览器
npm run dev:host # 添加 --host

npm run dev:host:open # 自动打开浏览器并添加 --host

```
3. 打包编译

```bash
npm run build
```
4. 预览

```bash
npm run preview

## 使用说明

待更新